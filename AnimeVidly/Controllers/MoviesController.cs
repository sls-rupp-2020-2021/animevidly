﻿using Microsoft.AspNetCore.Mvc;
using AnimeVidly.Models;

namespace AnimeVidly.Controllers
{
    public class MoviesController : Controller
    {
        public IActionResult Random()
        {
            var movie = new Movie() { Name = "Kimetsu no Yaibasa" };
            return View(movie);
        }

        public ActionResult ByReleaseDate (int year,int month)
        {
            return Content(year + "/" + month);
        }
        
    }
}
